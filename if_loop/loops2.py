# Range ------------------------------------
# x = range(100)
# y = []
# 
# for numero in x:
#     y.append(numero)
# 
# print(y)

# Dos listas ---------------------------
# x = [2, 4, 6, 8]
# y = [3, 5, 7, 9]
# z = []
# 
# for i in range(len(x)):
#     z.append(x[i] * y[i])
# 
# print(z)

# zip -------------------------------
# x = [2, 4, 6, 8]
# y = [3, 5, 7, 9]
# z = []
# 
# for xi, yi in zip(x, y):
#     z.append(xi * yi)
# 
# print(z)

# Loop sobre cadena ---------------------
# print("Alvin")
# 
# for letra in "Alvin":
#     print(letra)
