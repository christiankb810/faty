#
# Indices ------------------------
# x = [1, 2, 3, 4, 5]
# for i in range(len(x)):
#     x[i] = 2 * x[i]
# 
# print(x)

# Otra lista --------------------------------
# x = [1, 2, 3, 4, 5]
# y = []
# 
# for numero in x:
#     y.append(2 * numero)
# 
# print(y)

# enumerate --------------------------------
# x = [1, 2, 3, 4, 5]
# 
# for i, numero in enumerate(x):
#     x[i] = 2 * numero
# 
# print(x)
